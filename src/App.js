import React from 'react';
import CustomerList from "./CustomerList";
import Header from "./Header";

const App = () => {
  console.log(process.env.NODE_ENV)
  console.log(process.env.REACT_APP_CUSTOMER_API_URL)
  return (
    <>
    <Header/>
    <br></br>
    <br></br>
    <CustomerList/>
    </>
  );
}

export default App;
