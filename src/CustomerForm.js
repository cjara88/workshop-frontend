import React, { useEffect } from 'react';
import axios from 'axios';
import Button from "@material-ui/core/Button";
import { useForm } from "react-hook-form";
import Input from "@material-ui/core/Input";

const BASE_URL = `http://localhost:8080/customers`;

const CustomerForm = ({ customer, saveSuccess }) => {
  const { register, handleSubmit, errors, setValue, reset } = useForm();

  const onSubmit = (data, e) => {
    e.preventDefault();
    if (customer.id) {
      data.id = customer.id;
      axios.put(BASE_URL, {
        id: data.id,
        firstName: data.firstName,
        lastName: data.lastName,
        phoneNumber: data.phoneNumber,
        email: data.email
      }).then(response => {
        reset({});
        alert("Se actualizó exitosamente.")
      }).catch(error => {
        console.log(error.response);
        alert("Ocurrió un error inesperado al guardar.")
      });
    } else {
      axios.post(BASE_URL, {
        firstName: data.firstName,
        lastName: data.lastName,
        phoneNumber: data.phoneNumber,
        email: data.email
      }).then(response => {
        reset({});
        alert("Se guardó exitosamente.")
      }).catch(error => {
        console.log(error.response);
        let errorResponse = error.response;
        if (errorResponse.status === 500) {
          alert(errorResponse.data.message);
        } else {
          alert("Ocurrió un error inesperado al guardar.");
        }
      })
    }
    saveSuccess();
  };

  useEffect(() => {
    if (customer) {
      setValue("firstName", customer.firstName);
      setValue("lastName", customer.lastName);
      setValue("phoneNumber", customer.phoneNumber);
      setValue("email", customer.email);
    }
  }, [customer, setValue]);


  return (
    <form onSubmit={handleSubmit(onSubmit)} noValidate>
      <Input
        name="firstName"
        inputRef={register({ required: true })}
        placeholder="First Name"
        error={!!errors.firstName}
        fullWidth
      />
      <p style={{ color: "red" }}>{errors.firstName && "First Name is required"}</p>
      <Input
        name="lastName"
        inputRef={register({ required: true })}
        placeholder="Last Name"
        error={!!errors.lastName}
        fullWidth
      />
      <p style={{ color: "red" }}>{errors.lastName && "Last Name is required"}</p>
      <Input
        name="phoneNumber"
        inputRef={register({ required: true })}
        placeholder="Phone Number"
        error={!!errors.phoneNumber}
        fullWidth
      />
      <p style={{ color: "red" }}>{errors.phoneNumber && "Phone Number is required"}</p>
      <Input
        name="email"
        inputRef={register({ required: true })}
        placeholder="Email"
        error={!!errors.email}
        fullWidth
      />
      <p style={{ color: "red" }}>{errors.email && "Email is required"}</p>

      <Button
        variant="contained"
        color="default"
        onClick={() => { reset({}) }}
      >
        Reset
                  </Button>
      <Button
        type="submit"
        variant="contained"
        color="primary"
      >
        Save
                  </Button>
    </form>
  );
}

export default CustomerForm;