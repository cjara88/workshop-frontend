import React, { useEffect, useState } from 'react';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import axios from 'axios';
import Grid from "@material-ui/core/Grid";
import Tooltip from "@material-ui/core/Tooltip";
import EditIcon from '@material-ui/icons/Edit';
import Box from "@material-ui/core/Box";
import CustomerForm from "./CustomerForm";
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import Typography from "@material-ui/core/Typography";
import DeleteIcon from '@material-ui/icons/Delete';

const BASE_URL = process.env.REACT_APP_CUSTOMER_API_URL;

const CustomerList = () => {
    //paginado de consulta
    const [customerPage, setCustomerPage] = useState({
        content: [],
        totalElements: 0
    });
    //pagina actual
    const [page, setPage] = useState(0);
    //cantidad de filas por pagina
    const [rowsPerPage, setRowsPerPage] = useState(5);
    //customer seleccionado
    const [customerSelected, setCustomerSelected] = useState({});

    useEffect(() => {
        loadData({});
    }, []);

    const editCustomer = (customerSelected) => {
        setCustomerSelected(customerSelected);
    }

    const deleteCustomer = (customerSelected) => {
        axios.delete(BASE_URL + "/" + customerSelected.id).then(response => {
            alert("Se eliminó exitosamente.");
            loadData({});
        })
    }

    /*Se ejecuta cuando solicito la siguiente pagina, 
    o una pagina determinada */
    const handleChangePage = async (event, newPage) => {
        setCustomerPage({ ...customerPage, content: [] });
        loadData({ page: newPage, rowsPerPage: rowsPerPage });
        setPage(newPage);
    }

    const handleFormSave = () => {
        setPage(0);
        setRowsPerPage(5);
        loadData({});
    }

    /*Se ejecuta cuando solicito un cambio en la cantidad 
    de registros por pagina */
    const handleChangeRowsPerPage = (event) => {
        //limpiamos los datos actuales
        setCustomerPage({ ...customerPage, content: [] });
        let pageSize = parseInt(event.target.value, 10);
        //vuelve a traer los datos
        loadData({rowsPerPage: pageSize})
        //seteamos el nuevo valor de registros por pagina
        setRowsPerPage(pageSize);
    }

    const loadData = async ({ page = 0, rowsPerPage = 5 }) => {
        try {
            const response = await axios.get(BASE_URL + "/page", {
                params: {
                    page: page,
                    pageSize: rowsPerPage
                }
            });
            let data = response.data;
            setCustomerPage({ ...customerPage, content: data.content, totalElements: data.totalElements });
        } catch (error) {
            console.error(error);
            //toast.error("Error al obtener datos de documentos del cliente.");
        }
    };


    return (
        <>
            <Grid container spacing={3}>
                <Grid item xs={4}>
                    <Box p={3}>
                        <Typography variant="h5" align="center">
                            Customer Form
                    </Typography>
                        <CustomerForm customer={customerSelected} saveSuccess={handleFormSave} />
                    </Box>
                </Grid>
                <Grid item xs={8}>
                    <Typography variant="h5" align="center">
                        List Customer
                    </Typography>
                    <TableContainer component={Paper}>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell align="left">Id</TableCell>
                                    <TableCell align="left">First Name</TableCell>
                                    <TableCell align="left">Last Name</TableCell>
                                    <TableCell align="left">Phone Number</TableCell>
                                    <TableCell align="left">Email</TableCell>
                                    <TableCell align="center">Actions</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {customerPage.content.map(customer => (
                                    <TableRow key={customer.id}>
                                        <TableCell align="left">{customer.id}</TableCell>
                                        <TableCell align="left">{customer.firstName}</TableCell>
                                        <TableCell align="left">{customer.lastName}</TableCell>
                                        <TableCell align="left">{customer.phoneNumber}</TableCell>
                                        <TableCell align="left">{customer.email}</TableCell>
                                        <TableCell align="center">
                                            <Tooltip title="Edit" aria-label="edit">
                                                <IconButton
                                                    onClick={() => editCustomer(customer)}
                                                    aria-label="delete"
                                                    size="small"
                                                    color="primary"
                                                >
                                                    <EditIcon />
                                                </IconButton>
                                            </Tooltip>
                                            <Tooltip title="Delete" aria-label="delete">
                                                <IconButton
                                                    onClick={() => deleteCustomer(customer)}
                                                    aria-label="delete"
                                                    size="small"
                                                    color="secondary"
                                                >
                                                    <DeleteIcon />
                                                </IconButton>
                                            </Tooltip>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                            <TableFooter>
                                <TableRow>
                                    <TablePagination
                                        rowsPerPageOptions={[5,10, 15, 20]}
                                        colSpan={12}
                                        count={customerPage.totalElements}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        SelectProps={{
                                            inputProps: { 'aria-label': 'Registros por página' },
                                            native: true,
                                        }}
                                        labelDisplayedRows={({ from, to, count }) => `${from}-${to} de ${count}`}
                                        labelRowsPerPage='Registros por página'
                                        onChangePage={handleChangePage}
                                        onChangeRowsPerPage={handleChangeRowsPerPage}
                                    />
                                </TableRow>
                            </TableFooter>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>

        </>
    );
}

export default CustomerList;