import React from 'react';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from '@material-ui/core/IconButton';
import PeopleIcon from '@material-ui/icons/People';

const Header = () => {
  return (
      <AppBar color="primary" position="static">
        <Toolbar>
          <IconButton edge="start" color="inherit" href="/" aria-label="menu">
            <PeopleIcon/>
          </IconButton>
          <Typography variant="h5" color="inherit">
            Customers
      </Typography>
        </Toolbar>
      </AppBar>  
  );
}

export default Header;
