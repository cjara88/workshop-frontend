#   workshop frontend

_Repositorio de ejemplo de CRUD realizado en el workshop de frontend con React_

Para la contrucción del mismo se utilzaron las siguientes librerias

* [React](https://es.reactjs.org/)
* [Materil-Ui](https://material-ui.com/)
* [Axios](https://github.com/axios/axios)
* [React-hook-form](https://react-hook-form.com/)


## Pre-requisitos

* [Nodejs](https://nodejs.org/es/download/) v10.16.0+
* [Yarn](https://yarnpkg.com/en/docs/install#centos-stable) 1.17.3+
* [Serve](https://github.com/zeit/serve) 11.2.0+
* Desplegar la aplicacion customer-app

## Configuraciones

* Setear los valores correspondientes en los archivos de propiead de acuerdo al entorno

```
.env : local
.env.production : produccion

```

### Deploy
Paso 1 
* Instalar todos las dependencias necesarias para el proyecto.
```
yarn install

```

Paso 2
* De acuerdo al entorno en donde va ser desplegado,ejecutar :
#### Local


```
yarn start

```

#### Produccion
Paso 1 
```
yarn run build
```
Paso 2
```
serve -s build
```
